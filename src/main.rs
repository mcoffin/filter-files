use std::{
    fs,
    io,
    path::Path,
};

#[inline(never)]
fn is_file<P: AsRef<Path>>(path: P) -> bool {
    match fs::metadata(path) {
        Ok(metadata) => metadata.is_file(),
        Err(_) => false,
    }
}

fn main() {
    use io::BufRead;
    let stdin = io::stdin();
    let stdin = stdin.lock();
    let mut files = stdin.lines();
    while let Some(Ok(line)) = files.next() {
        if is_file(&line) {
            println!("{}", &line);
        }
    }
}
